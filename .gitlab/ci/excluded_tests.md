## ci-common

- `lv` -> LidarView ci doesn't support UI tests on all platforms.
- `Python` -> Segfault need investigation.

## ci-fedora35-debug

- `lv` -> Segfault with `xvfb-run` needs investigation.
- `TestVelodyneHDLPositionReader` -> doesn't find proj.db, gdal issue?
